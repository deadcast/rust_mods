﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("TweetPlugin", "deadcast64", 0.1)]
    [Description("Tweet your life from Rust")]

    class TweetPlugin : RustPlugin
    {
        private DateTime nextCheckTime = DateTime.Now;

        private void OnPlayerSleepEnded(BasePlayer player)
        {
            Tweet(player, "😀 hello i just woke up and i'm currently at " + player.GetEstimatedWorldPosition().ToString());
        }

        private void OnRunPlayerMetabolism(PlayerMetabolism metabolism)
        {
            if (nextCheckTime < DateTime.Now)
            {
                var player = metabolism.GetComponent<BasePlayer>();
                var health = "💊" + player.health;
                var bleeding = ", 💉" + metabolism.bleeding.value;
                var calories = ", 🍔" + metabolism.calories.value;
                var heartrate = ", 💗" + metabolism.heartrate.value;
                var hydration = ", 💧" + metabolism.hydration.value;
                var temperature = ", 🔆" + metabolism.temperature.value;
                Tweet(player, "[vitals] " + health + bleeding + calories + heartrate + hydration + temperature);

                nextCheckTime = DateTime.Now.AddMinutes(30);
            }
        }

        private void OnEntityDeath(BaseEntity entity, HitInfo hit)
        {
            if (entity is BasePlayer)
            {
                var player = (BasePlayer)entity;

                if (hit.Initiator == null)
                {
                    Tweet(player, "😪 i just died at " + player.GetEstimatedWorldPosition().ToString());
                }
                else
                {
                    if (hit.Initiator is BasePlayer)
                    {
                        var attacker = (BasePlayer)hit.Initiator;
                        Tweet(player, "😡😡😡 shit i just got killed by " + attacker.displayName);
                    }
                    else
                    {
                        Tweet(player, "a fricken " + hit.Initiator.LookupShortPrefabName().Replace(".prefab", "") + " just killed me 😖");
                    }
                }
            }
            else
            {
                if (hit.Initiator is BasePlayer)
                {
                    var attacker = (BasePlayer)hit.Initiator;
                    Tweet(attacker, "🔫 heh i just killed a " + entity.LookupShortPrefabName().Replace(".prefab", "") + " 🔫");
                }
            }
        }

        private void Tweet(BasePlayer player, string status)
        {
            if (player.displayName.Equals("Uncle Steve"))
            {
                webrequest.EnqueuePost("https://gx4ve6jpzb.execute-api.us-east-1.amazonaws.com/prod/tweet",
                    "{\"status\":\"" + status + "\"}", (code, resp) => { }, this);
            }
        }
    }
}
