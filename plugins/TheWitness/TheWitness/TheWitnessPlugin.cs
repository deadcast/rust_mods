﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("The Witness", "deadcast64", 0.1)]
    [Description("Make puzzles just like in The Witness!")]

    public class TheWitnessPlugin : RustPlugin
    {
        public enum BlockTypes { Path, Rule, Entrance, Exit }
        public enum RuleTypes { None, Black_Circle, White_Circle }
        public enum SolveActionTypes { None, Open_Door, Give_Loot }

        private Dictionary<ulong, Puzzle> pendingSolveActionFinish = new Dictionary<ulong, Puzzle>();
        private static List<Puzzle> puzzles = new List<Puzzle>();

        public class Puzzle
        {
            public Door door;
            public SolveActionTypes solveAction = SolveActionTypes.None;

            private Dictionary<int, FoundationInfo> foundations = new Dictionary<int, FoundationInfo>();
            private Stack<BuildingBlock> playerPath = new Stack<BuildingBlock>();
            private bool gameStarted = false;
            private TheWitnessPlugin plugin;
            private BasePlayer currentPlayer = null;

            public static Puzzle FindByPosition(Vector3 pos)
            {
                foreach (var puzzle in puzzles)
                {
                    if (puzzle.FindBlock(pos))
                    {
                        return puzzle;
                    }
                }

                return null;
            }

            public static bool RegisterAtPosition(TheWitnessPlugin plugin, Vector3 pos)
            {
                if (FindByPosition(pos) == null)
                {
                    var newPuzzle = new Puzzle(plugin, pos);

                    if (newPuzzle.IsValid())
                    {
                        puzzles.Add(newPuzzle);
                        newPuzzle.UpgradeBoard();
                        return true;
                    }
                }

                return false;
            }

            public Puzzle(TheWitnessPlugin plugin, Vector3 pos)
            {
                this.plugin = plugin;

                FindConnectedFoundations(pos);
            }

            public void Destroy()
            {
                foreach (var foundation in foundations)
                {
                    SetGrade(foundation.Value.self, BuildingGrade.Enum.Twigs);
                }
                
                puzzles.Remove(this);
            }

            public FoundationInfo GetBlockInfo(BuildingBlock block)
            {
                if (!block) return null;
                var id = block.GetInstanceID();
                if (foundations.ContainsKey(id)) return foundations[block.GetInstanceID()];
                return null;
            }

            public bool IsReady()
            {
                var entrance = foundations.Count(x => x.Value.type == BlockTypes.Entrance);
                var exit = foundations.Count(x => x.Value.type == BlockTypes.Exit);
                return entrance == 1 && exit == 1;
            }

            public BuildingBlock FindBlock(Vector3 pos, bool checkIfRegistered = true)
            {
                RaycastHit hit;

                if (!Physics.Raycast(pos + Vector3.up, Vector3.down, out hit, 10.0f, Rust.Layers.Server.Buildings)) return null;

                var block = hit.GetEntity().GetComponent<BuildingBlock>();

                if (!block.LookupShortPrefabName().Contains("foundation")) return null;

                if (checkIfRegistered)
                {
                    if (!foundations.ContainsKey(block.GetInstanceID())) return null;
                }

                return block;
            }

            private bool IsValid()
            {
                // check to make sure that the foundation is rectangular and filled
                bool validDesign = (foundations.Where(x => x.Value.Neighbors.Count == 2).Count() == 4);
                bool validAmount = (foundations.Count() % 2 != 0) && (foundations.Count() != 1);

                return validDesign && validAmount;
            }

            private void GatherBlocks(Vector3 origin, List<FoundationInfo> collection)
            {
                var block = FindBlock(origin);
                if (!block) return;
                if (playerPath.Contains(block)) return;

                var blockInfo = GetBlockInfo(block);
                if (collection.Contains(blockInfo)) return;

                collection.Add(blockInfo);

                // translate up a little for a clean raycast check for subsequent foundation blocks
                var blockPos = block.transform.position + new Vector3(0, 1, 0);
                var forwardZ = block.transform.forward * block.bounds.extents.z * 2;
                var rightX = block.transform.right * block.bounds.extents.x * 2;

                // check sides around foundation block for more foundations
                GatherBlocks(blockPos + forwardZ, collection);
                GatherBlocks(blockPos + rightX, collection);
                GatherBlocks(blockPos - forwardZ, collection);
                GatherBlocks(blockPos - rightX, collection);
            }

            private BuildingBlock FindConnectedFoundations(Vector3 origin)
            {
                var block = FindBlock(origin, false);

                if (!block) return null;
                if (foundations.ContainsKey(block.GetInstanceID())) return block;

                // translate up a little for a clean raycast check for subsequent foundation blocks
                var blockPos = block.transform.position + new Vector3(0, 1, 0);
                var forwardZ = block.transform.forward * block.bounds.extents.z * 2;
                var rightX = block.transform.right * block.bounds.extents.x * 2;
                var info = new FoundationInfo { self = block };

                foundations.Add(block.GetInstanceID(), info);

                // check sides around foundation block for more foundations
                var front = FindConnectedFoundations(blockPos + forwardZ);
                info.AddNeighbor(front);

                var right = FindConnectedFoundations(blockPos + rightX);
                info.AddNeighbor(right);

                var back = FindConnectedFoundations(blockPos - forwardZ);
                info.AddNeighbor(back);

                var left = FindConnectedFoundations(blockPos - rightX);
                info.AddNeighbor(left);

                return block;
            }

            private void SetGrade(BuildingBlock block, BuildingGrade.Enum grade)
            {
                block.SetGrade(grade);
                block.SetHealthToMax();
                block.SendNetworkUpdate(BasePlayer.NetworkQueue.Update);
            }

            private void CallSetSolveAction()
            {
                switch (solveAction)
                {
                    case SolveActionTypes.Open_Door:
                        OpenDoorAction();
                        break;
                    case SolveActionTypes.Give_Loot:
                        GiveLootAction();
                        break;
                    default:
                        break;
                }
            }

            private void OpenDoorAction()
            {
                if (door)
                {
                    door.SetFlag(BaseEntity.Flags.Open, true);
                }
            }

            private void GiveLootAction()
            {
                if (!currentPlayer) return;

                var item = ItemManager.CreateByName("rifle.bolt");
                item.CreateWorldObject(currentPlayer.transform.position, currentPlayer.transform.rotation);
            }

            private void ResetBoard()
            {
                if (playerPath.Count > 0)
                {
                    foreach (var node in playerPath)
                    {
                        SetGrade(node, BuildingGrade.Enum.Metal);
                    }

                    playerPath.Clear();
                }

                currentPlayer = null;
            }

            private void UpgradeBoard()
            {
                foreach (var foundation in foundations)
                {
                    SetGrade(foundation.Value.self, BuildingGrade.Enum.Metal);
                }

                DetectRuleFoundations();
            }

            private void DetectRuleFoundations()
            {
                var corner = foundations.Where(x => x.Value.Neighbors.Count == 2).First();
                var startBlock = corner.Value.Neighbors.First();
                var startBlockInfo = GetBlockInfo(startBlock);
                var blockDistance = Vector3.Distance(startBlock.transform.position, corner.Value.self.transform.position);
                Vector3 xDir = Vector3.zero;
                Vector3 zDir = Vector3.zero;

                foreach (var block in startBlockInfo.Neighbors)
                {
                    var info = GetBlockInfo(block);

                    // dir for next row to process
                    if (info.Neighbors.Count == 3)
                    {
                        xDir = (block.transform.position - startBlock.transform.position).normalized;
                    }

                    // dir for next block in row to be processed
                    if (info.Neighbors.Count == 4)
                    {
                        zDir = (block.transform.position - startBlock.transform.position).normalized;
                    }
                }

                var stepDirX = xDir * blockDistance;
                var stepDirZ = zDir * blockDistance;
                var rowPosition = startBlock.transform.position;
                var blockPosition = Vector3.zero;
                var maxBoardWidth = Mathf.Ceil(Mathf.Sqrt(foundations.Count)); // rough size of game board

                // iterate through each row on the board processing each block to see if it should
                // be marked as a potential rule piece
                for (int i = 0; i < maxBoardWidth; i++)
                {
                    var currRow = FindBlock(rowPosition);
                    if (currRow == null) continue;

                    // only use outside wall blocks for next row selection
                    if (GetBlockInfo(currRow).Neighbors.Count == 3)
                    {
                        blockPosition = rowPosition + stepDirZ; // move up 1 before we begin scan

                        for (int j = 0; j < maxBoardWidth; j++)
                        {
                            var currRowBlock = FindBlock(blockPosition);
                            if (currRowBlock == null) continue;

                            var currItemBlockInfo = GetBlockInfo(currRowBlock);

                            // only select inner blocks
                            if (currItemBlockInfo.Neighbors.Count == 4)
                            {
                                SetGrade(currRowBlock, BuildingGrade.Enum.TopTier);
                                currItemBlockInfo.type = BlockTypes.Rule;
                            }

                            blockPosition += (stepDirZ * 2); // every other block
                        }
                    }

                    rowPosition += (stepDirX * 2); // every other row
                }
            }

            private bool RuleCheck()
            {
                var ruleBlocks = foundations.Where(x => x.Value.type == BlockTypes.Rule);
                List<bool> rulesPassed = new List<bool>();
                List<FoundationInfo> blocks;

                foreach (var block in ruleBlocks)
                {
                    switch (block.Value.rule)
                    {
                        case RuleTypes.White_Circle:
                            blocks = new List<FoundationInfo>();
                            GatherBlocks(block.Value.self.transform.position, blocks);
                            rulesPassed.Add(WhiteCircleRuleProcess(blocks));
                            break;
                        case RuleTypes.Black_Circle:
                            blocks = new List<FoundationInfo>();
                            GatherBlocks(block.Value.self.transform.position, blocks);
                            rulesPassed.Add(BlackCircleRuleProcess(blocks));
                            break;
                        default:
                            break;
                    }
                }

                return rulesPassed.TrueForAll(x => x == true);
            }

            private bool WhiteCircleRuleProcess(List<FoundationInfo> foundations)
            {
                foreach (var foundation in foundations)
                {
                    if (foundation.type == BlockTypes.Rule && foundation.rule != RuleTypes.White_Circle && foundation.rule != RuleTypes.None)
                    {
                        return false;
                    }
                }

                return true;
            }

            private bool BlackCircleRuleProcess(List<FoundationInfo> foundations)
            {
                foreach (var foundation in foundations)
                {
                    if (foundation.type == BlockTypes.Rule && foundation.rule != RuleTypes.Black_Circle && foundation.rule != RuleTypes.None)
                    {
                        return false;
                    }
                }

                return true;
            }

            private bool CheckForPlayersWaiting()
            {
                if (!currentPlayer)
                {
                    var players = BasePlayer.activePlayerList as List<BasePlayer>;
                    if (players.Count < 1) return false;

                    for (int i = 0; i < players.Count; i++)
                    {
                        if (FindBlock((players[i].transform.position)))
                        {
                            currentPlayer = players[i];
                            return true;
                        }
                    }

                    return false;
                }

                return true;
            }

            public void Update()
            {
                if (!IsReady()) return; // entrance and exit not yet defined so can't start

                if (!CheckForPlayersWaiting()) return;

                var block = FindBlock(currentPlayer.eyes.position);
                if (!block)
                {
                    // player jumped off or something so we should clear the board
                    PlaySound(currentPlayer, "locks/keypad/effects/lock.code.denied.prefab");
                    ResetBoard();
                    gameStarted = false;
                    return;
                }

                var blockInfo = GetBlockInfo(block);
                if (blockInfo.type == BlockTypes.Entrance && !gameStarted)
                {
                    gameStarted = true;
                    PlaySound(currentPlayer, "locks/keypad/effects/lock.code.lock.prefab");
                }

                if (!gameStarted) return;

                if (!playerPath.Contains(block))
                {
                    if (playerPath.Count < 1)
                    {
                        SetGrade(block, BuildingGrade.Enum.Stone);
                        playerPath.Push(block);
                    }
                    else
                    {
                        // only allow blocks placed by neighbors
                        var prevBlockInfo = GetBlockInfo(playerPath.Peek());
                        if (prevBlockInfo.Neighbors.Contains(block) && blockInfo.type != BlockTypes.Rule)
                        {
                            SetGrade(block, BuildingGrade.Enum.Stone);
                            playerPath.Push(block);
                        }
                    }
                }
                else
                {
                    if (playerPath.Peek() != block)
                    {
                        SetGrade(playerPath.Pop(), BuildingGrade.Enum.Metal);
                    }
                }

                if (blockInfo.type == BlockTypes.Exit && gameStarted)
                {
                    gameStarted = false;

                    if (RuleCheck())
                    {
                        PlaySound(currentPlayer, "locks/keypad/effects/lock.code.unlock.prefab");
                        CallSetSolveAction();
                    }
                    else
                    {
                        PlaySound(currentPlayer, "locks/keypad/effects/lock.code.denied.prefab");
                    }

                    ResetBoard();
                }
            }

            private void PlaySound(BasePlayer player, string path)
            {
                Effect.server.Run("assets/prefabs/" + path, player.eyes.position, Vector3.zero);
            }
        }

        public class FoundationInfo
        {
            public List<BuildingBlock> Neighbors { get { return neighbors; } }
            public BuildingBlock self;
            public BlockTypes type = BlockTypes.Path;
            public RuleTypes rule = RuleTypes.None;

            private List<BuildingBlock> neighbors = new List<BuildingBlock>();

            public bool AddNeighbor(BuildingBlock neighborBlock)
            {
                if (!neighborBlock) return false;

                if (!neighbors.Contains(neighborBlock))
                {
                    neighbors.Add(neighborBlock);
                    return true;
                }

                return false;
            }
        }

        private void OnTick()
        {
            for (int i = 0; i < puzzles.Count; i++)
            {
                puzzles[i].Update();
            }
        }

        [ConsoleCommand("tw.start")]
        private void Start(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            var block = puzzle.FindBlock(player.eyes.position);
            if (block)
            {
                var info = puzzle.GetBlockInfo(block);
                info.type = BlockTypes.Entrance;
            }
        }

        [ConsoleCommand("tw.end")]
        private void End(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            var block = puzzle.FindBlock(player.eyes.position);

            if (block)
            {
                var info = puzzle.GetBlockInfo(block);
                info.type = BlockTypes.Exit;
            }
        }

        [ConsoleCommand("tw.white")]
        private void WhiteRule(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            var block = puzzle.FindBlock(player.eyes.position);

            if (block)
            {
                puzzle.GetBlockInfo(block).rule = RuleTypes.White_Circle;
            }
        }

        [ConsoleCommand("tw.black")]
        private void BlackRule(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            var block = puzzle.FindBlock(player.eyes.position);

            if (block)
            {
                puzzle.GetBlockInfo(block).rule = RuleTypes.Black_Circle;
            }
        }

        [ConsoleCommand("tw.reg")]
        private void RegisterBoard(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();
            var result = Puzzle.RegisterAtPosition(this, player.eyes.position);

            if (result)
            {
                SendReply(player, "Board created!");
            }
            else
            {
                SendReply(player, "Board is either invalid or already registered.");
            }
        }

        [ConsoleCommand("tw.des")]
        private void DestroyBoard(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            puzzle.Destroy();
        }

        [ConsoleCommand("tw.sign")]
        private void AddSign(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();
            var prefabName = "assets/prefabs/deployable/signs/sign.medium.wood.prefab";

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            var block = puzzle.FindBlock(player.eyes.position);
            if (block)
            {
                var position = block.transform.position + new Vector3(0, 0.1f, 0);
                var rotation = block.transform.rotation * Quaternion.Euler(270, 0, 0);
                var sign = GameManager.server.CreateEntity(prefabName, position, rotation);

                sign.Spawn();
                sign.SendNetworkUpdate();
            }
        }

        [ConsoleCommand("tw.on_solve_give_loot")]
        private void OnSolveGiveLoot(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            var block = puzzle.FindBlock(player.eyes.position);
            if (!block) return;

            puzzle.solveAction = SolveActionTypes.Give_Loot;
        }

        [ConsoleCommand("tw.on_solve_open_door")]
        private void OnSolveOpenDoor(ConsoleSystem.Arg arg)
        {
            var player = arg.Player();

            var puzzle = Puzzle.FindByPosition(player.transform.position);
            if (puzzle == null) return;

            var block = puzzle.FindBlock(player.eyes.position);
            if (!block) return;

            puzzle.solveAction = SolveActionTypes.Open_Door;

            pendingSolveActionFinish[player.userID] = puzzle;
        }

        private void OnDoorOpened(Door door, BasePlayer player)
        {
            if (pendingSolveActionFinish.ContainsKey(player.userID))
            {
                var puzzle = pendingSolveActionFinish[player.userID];
                if (puzzle.solveAction == SolveActionTypes.Open_Door)
                {
                    SendReply(player, "Door successfully linked to puzzle!");
                    puzzle.door = door;
                }

                pendingSolveActionFinish.Remove(player.userID);
            }
        }

        public void DebugDrawLine(BasePlayer player, Color color, Vector3 start, Vector3 end)
        {
            player.SendConsoleCommand("ddraw.line", 10, color, start, end);
        }

        public void DebugDrawArrow(BasePlayer player, Color color, Vector3 start, Vector3 end)
        {
            player.SendConsoleCommand("ddraw.arrow", 10, color, start, end, 0.5f);
        }
    }
}
